using CEllipse.Classes;
using NUnit.Framework;

namespace CEllipse.Tests
{
    [TestFixture]
    public class EllipseOperations
    {
        private Ellipse _ellipse;
        
        public EllipseOperations()
        {
        }

        [SetUp]
        public void EccentricityDoesNotReturnExceptions()
        {
            Assert.DoesNotThrow(() => _ellipse = new Ellipse(20, 19, new Vector2D(1, 1)),
                "Ellipse b cannot be bigger than a");
        }

        [Test]
        public void EllipseEqualityOperation()
        {
            var ellipse1 = new Ellipse(20, 19, new Vector2D(1, 1));
            Assert.True(_ellipse == ellipse1, $"Ellipse does not equal {ellipse1}");
        }
        
        [Test]
        public void EllipseMoveToVector()
        {
            var vector = new Vector2D(2, 3);
            var newEllipse = _ellipse + vector;
            Assert.True(newEllipse.Center.X > vector.X && newEllipse.Center.Y > vector.Y, $"Ellipse does not moved to {vector}. Check comparision of X & Y");
        }

        [Test]
        public void EllipseTension()
        {
            const double tension = 1.5;
            var tensedEllipse = _ellipse * tension;
            Assert.True(tensedEllipse.A > _ellipse.A, $"Ellipse does not tensed by {tension}");
        }
        
        [Test]
        public void EllipseCompression()
        {
            const double compression = 0.3;
            var compressedEllipse = _ellipse * compression;
            Assert.True(compressedEllipse.A < _ellipse.A && compressedEllipse.B < _ellipse.B, $"Ellipse does not compressed by {compression}");
        }
    }
}