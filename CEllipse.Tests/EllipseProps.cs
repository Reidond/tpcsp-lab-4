using CEllipse.Classes;
using NUnit.Framework;

namespace CEllipse.Tests
{
    [TestFixture]
    public class EllipseProps
    {
        private Ellipse _ellipse;
        
        public EllipseProps()
        {
        }

        [SetUp]
        public void EccentricityDoesNotReturnExceptions()
        {
            Assert.DoesNotThrow(() => _ellipse = new Ellipse(20, 19, new Vector2D(1, 1)),
                "Ellipse b cannot be bigger than a");
        }

        [Test]
        public void EllipseAMustBeGreaterThanB()
        {
            Assert.Greater(_ellipse.A, _ellipse.B, "Ellipse B cannot be greater than ellipse A");
        }
    }
}