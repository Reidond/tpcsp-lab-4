using System;
using System.IO;
using System.Xml.Serialization;
using CEllipse.Classes;
using NUnit.Framework;

namespace CEllipse.Tests
{
    [TestFixture]
    public class EllipseXml
    {
        private readonly Ellipse _ellipse;
        private readonly string _xmlPath;

        public EllipseXml()
        {
            _ellipse = new Ellipse(20, 19, new Vector2D(1, 1));
            _xmlPath = @"/home/reidond/Documents/Ellipse.xml";
        }

        [Test]
        public void EllipseWriteXml()
        {
            // Even if this particular class doesnt have [Serializable] it passes
            Assert.DoesNotThrow(() => WriteXml(_ellipse, _xmlPath), "System.InvalidOperationException: There is an error in XML document");
        }
        
        [Test]
        public void EllipseReadXml()
        {
            // Even if this particular class doesnt have [Serializable] it passes
            Assert.DoesNotThrow(() => ReadXml(_xmlPath), "System.InvalidOperationException: There is an error in XML document");
        }

        private static void WriteXml(Ellipse ellipseInstance, string xmlFilePath)
        {
            var writer = new XmlSerializer(typeof(Ellipse));
            using (var file = File.OpenWrite(xmlFilePath))
            {
                writer.Serialize(file, ellipseInstance);
            }
        }
        
        private static Ellipse ReadXml(string xmlFilePath)
        {
            var reader = new XmlSerializer(typeof(Ellipse));
            using (var input = File.OpenRead(xmlFilePath))
            {
                return reader.Deserialize(input) as Ellipse;
            }
        }
    }
}