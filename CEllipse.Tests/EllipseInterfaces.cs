using CEllipse.Classes;
using NUnit.Framework;

namespace CEllipse.Tests
{
    [TestFixture]
    public class EllipseInterfaces
    {
        private Ellipse _ellipse;
        
        public EllipseInterfaces()
        {
        }

        [SetUp]
        public void EccentricityDoesNotReturnExceptions()
        {
            Assert.DoesNotThrow(() => _ellipse = new Ellipse(20, 19, new Vector2D(1, 1)),
                "Ellipse b cannot be bigger than a");
        }

        [Test]
        public void EllipseCloneIsEqualToNonClonedEllipse()
        {
            var ellipseClone = _ellipse.Clone();
            Assert.True(_ellipse.Equals(ellipseClone), $"{_ellipse} does not equals to {ellipseClone}");
        }
    }
}