using CEllipse.Classes;
using NUnit.Framework;

namespace CEllipse.Tests
{
    [TestFixture]
    public class EllipseMethods
    {
        private Ellipse _ellipse;
        
        public EllipseMethods()
        {
        }

        [SetUp]
        public void EccentricityDoesNotReturnExceptions()
        {
            Assert.DoesNotThrow(() => _ellipse = new Ellipse(20, 19, new Vector2D(1, 1)),
                "Ellipse b cannot be bigger than a");
        }

        [Test]
        public void EllipseArea()
        {
            Assert.DoesNotThrow(() => { var area = _ellipse.Area(); }, "Ellipse trowed some error. Possibly StackOverflowException");
        }
        
        [Test]
        public void EllipseContains()
        {
            var somePoint = new Point(3, 4);
            Assert.True(_ellipse.Contains(somePoint), "Ellipse does not contain Point(3,4)");
        }
    }
}