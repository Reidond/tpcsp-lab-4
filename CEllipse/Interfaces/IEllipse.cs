﻿using CEllipse.Classes;

namespace CEllipse.Interfaces
{
    public interface IEllipse
    {
        decimal Eccentricity { get; }

        double Area();

        bool Contains(Point point);
    }
}
