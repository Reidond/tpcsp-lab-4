﻿using System;
using CEllipse.Interfaces;
using CEllipse.BaseClasses;

namespace CEllipse.Classes
{
    [Serializable]
    public class Ellipse : TwoDiameterFigure, IEllipse, IEquatable<Ellipse>, ICloneable
    {
        public override int GetHashCode()
        {
            return Eccentricity.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Ellipse)obj);
        }

        public decimal Eccentricity { get; }

        public Ellipse(double a, double b, Vector2D center) : base(a, b, center)
        {
            if (b > a)
            {
                throw new ArgumentException("Ellipse b cannot be bigger than a", nameof(b));
            }
            Eccentricity = CalculateEccentricity();
        }

        public Ellipse()
        {
            Eccentricity = CalculateEccentricity();
        }

        public double Area() => Math.PI * A * B;

        public bool Contains(Point point) =>
            DecimalMath.Pow((decimal)point.X, 2) / DecimalMath.Pow((decimal)A, 2) + 
            DecimalMath.Pow((decimal)point.Y, 2) / DecimalMath.Pow((decimal)B, 2) <= 1;

        public object Clone() => new Ellipse(A, B, Center);

        public bool Equals(Ellipse other) => A == other.A && B == other.B && Center == other.Center;

        private decimal CalculateEccentricity() =>
            DecimalMath.Sqrt(1 - DecimalMath.Pow((decimal)B / (decimal)A, 2));

        public static Ellipse operator +(Ellipse a, Vector2D b) => new Ellipse(a.A, a.B,
            new Vector2D(a.Center.X + b.X, a.Center.Y + b.Y));

        public static bool operator ==(Ellipse a, Ellipse b) => a.A == b.A && a.B == b.B && a.Center == b.Center;

        public static bool operator !=(Ellipse a, Ellipse b) => !(a == b);

        public static Ellipse operator *(Ellipse a, double k)
        {
            if (k < 0)
            {
                throw new ArgumentException("Second argument cannot be negative", nameof(k));
            }
            return new Ellipse(a.A * k, a.B * k, a.Center);
        }
        
        public override string ToString() => $"Ellipse({A}, {B}, {Center})";
    }
}
