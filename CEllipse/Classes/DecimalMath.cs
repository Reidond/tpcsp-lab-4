using System;

namespace CEllipse.Classes
{
    public static class DecimalMath
    {
        public static decimal Sqrt(decimal x, decimal? guess = null)
        {
            var ourGuess = guess.GetValueOrDefault(x / 2m);
            var result = x / ourGuess;
            var average = (ourGuess + result) / 2m;

            return average == ourGuess ? average : Sqrt(x, average);
        }

        public static decimal Pow(decimal var1, decimal var2) => (decimal) Math.Pow((double) var1, (double) var2);
    }
}