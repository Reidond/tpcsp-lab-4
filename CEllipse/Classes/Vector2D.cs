﻿namespace CEllipse.Classes
{
    public class Vector2D
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Vector2D(double x, double y)
        {
            X = x;
            Y = y;
        }

        public Vector2D()
        {
            X = 0;
            Y = 0;
        }
        
        public static bool operator ==(Vector2D a, Vector2D b) => a.X == b.X && a.Y == b.Y;

        public static bool operator !=(Vector2D a, Vector2D b) => !(a == b);

        public override string ToString() => $"Vector({X}, {Y})";
    }
}
