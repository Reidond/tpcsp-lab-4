﻿using CEllipse.Classes;

namespace CEllipse.BaseClasses
{
    public class TwoDiameterFigure
    {
        public double A { get; set; }
        public double B { get; set; }
        public Vector2D Center { get; set; }

        public TwoDiameterFigure(double a, double b, Vector2D center)
        {
            A = a;
            B = b;
            Center = center;
        }

        public TwoDiameterFigure(double A, double B)
        {
            this.A = A;
            this.B = B;
            Center = new Vector2D(0, 0);
        }

        /* A must be greater than B
           A & B must be not zero
         */
        public TwoDiameterFigure()
        {
            A = 2;
            B = 1;
            Center = new Vector2D(0, 0);
        }
    }
}
