﻿namespace TPCSP_Lab_4_Console
{
    public class TwoDiameterFigure
    {
        public double A { get; set; }
        public double B { get; set; }
        public Vector2d Center { get; set; }

        public TwoDiameterFigure(double A, double B, Vector2d Center)
        {
            this.A = A;
            this.B = B;
            this.Center = Center;
        }

        public TwoDiameterFigure(double A, double B)
        {
            this.A = A;
            this.B = B;
            Center = new Vector2d(0, 0);
        }

        public TwoDiameterFigure()
        {
            A = 0;
            B = 0;
            Center = new Vector2d(0, 0);
        }
    }
}
