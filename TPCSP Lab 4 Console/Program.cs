﻿using System.IO;
using System.Xml.Serialization;
using static System.Console;

namespace TPCSP_Lab_4_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            Ellipse ellipse = new Ellipse();
            ellipse.A = 10;
            ellipse.B = 15;
            ellipse.Center = 1;
            WriteXML(ellipse, @"C:\Users\Reidond\Documents\Ellipse.xml");

            Ellipse ellipse1 = ReadXML(@"C:\Users\Reidond\Documents\Ellipse.xml");
            WriteLine($"{ellipse1.A}\n{ellipse1.B}\n{ellipse1.Center}\n{ellipse1.Eccentricity}");
        }

        public static void WriteXML(Ellipse ellipseInstance, string xmlFilePath)
        {
            XmlSerializer writer = new XmlSerializer(typeof(Ellipse));
            using (FileStream file = File.OpenWrite(xmlFilePath))
            {
                writer.Serialize(file, ellipseInstance);
            }
        }
        public static Ellipse ReadXML(string xmlFilePath)
        {
            XmlSerializer reader = new XmlSerializer(typeof(Ellipse));
            using (FileStream input = File.OpenRead(xmlFilePath))
            {
                return reader.Deserialize(input) as Ellipse;
            }
        }
    }
}
