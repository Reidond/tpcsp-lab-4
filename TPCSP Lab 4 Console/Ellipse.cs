﻿using System;

namespace TPCSP_Lab_4_Console
{
    [Serializable()]
    public class Ellipse : TwoDiameterFigure, IEllipse, IEquatable<Ellipse>, ICloneable
    {
        public double Eccentricity { get; }

        public Ellipse(double A, double B, Vector2d Center) : base(A, B, Center)
        {
            Eccentricity = CalculateEccentricity();
        }

        public Ellipse() : base()
        {
            Eccentricity = CalculateEccentricity();
        }

        public double Area() => Math.PI * A * B;

        public bool Contains(Point point) => ((Math.Pow(point.X, 2) / Math.Pow(A, 2)) + (Math.Pow(point.Y, 2) / Math.Pow(B, 2))) <= 1;

        public object Clone() => new Ellipse(A, B, Center);

        public bool Equals(Ellipse other) => A == other.A
                                             && B == other.B
                                             && Center == other.Center;

        private double CalculateEccentricity() => Math.Sqrt(1 - Math.Pow(B / A, 2));

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public static Ellipse operator +(Ellipse a, Vector2d b)
        {
            return new Ellipse(a.A, a.B, new Vector2d(b.X, b.Y));
        }

        public static bool operator ==(Ellipse a, Ellipse b)
        {
            return a.A == b.A && a.B == b.B && a.Center == b.Center;
        }

        public static bool operator !=(Ellipse a, Ellipse b)
        {
            return !(a == b);
        }

        public static Ellipse operator *(Ellipse a, double b)
        {
            if (b > 0)
            {
                return new Ellipse(a.A, a.B + b, a.Center);
            }
            else
            {
                return new Ellipse(a.A + b, a.B, a.Center);
            }
        }
    }
}
