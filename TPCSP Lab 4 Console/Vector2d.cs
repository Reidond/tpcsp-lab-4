﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPCSP_Lab_4_Console
{
    public class Vector2d
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Vector2d(double x, double y)
        {
            X = x;
            Y = y;
        }

        public Vector2d()
        {
            X = 0;
            Y = 0;
        }
    }
}
