﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TPCSP_Lab_4_Console
{
    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        public Point()
        {
            X = 0;
            Y = 0;
        }
    }
}
