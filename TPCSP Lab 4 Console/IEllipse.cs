﻿namespace TPCSP_Lab_4_Console
{
    public interface IEllipse
    {
        double Eccentricity { get; }

        double Area();

        bool Contains(Point point);
    }
}
